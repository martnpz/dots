###-> Fish config <-###

## Scripts path.
export PATH="/usr/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/plugs:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"

## Default editor.
export EDITOR='nvim'
export VISUAL='nvim'

## Man pages colorized (require most).
export PAGER="most"

## Start neofetch.
neofetch

## Spark rainbow.
#seq 70 | sort -R | spark | rainbow
# Get rainbow from https://github.com/arsham/rainbow/releases

## Starship prompt.
starship init fish | source

## Binds
#bind \cd backward-kill-word

# Default shell for fzf
export SHELL='/bin/sh'

## Set the cursor shapes for the different vi modes.
set fish_cursor_default     line        blink
set fish_cursor_insert      underscore  blink
set fish_cursor_replace_one underscore  blink
set fish_cursor_visual      line        blink

## Variables.
set fish_greeting                               # Disable welcome message.
set EDITOR "nvim"                               # Default editor.
#set VISUAL "emacs"                              # Graphic editor.
set fish_color_command '#9FE57A'                # Command color.
set fish_color_param '#EAEAEA'
set fi2sh_color_autosuggestion '#a3a8a5'

## Fuzzy Finder fzf.
export FZF_DEFAULT_COMMAND="fd -H"

## Aliases.
alias ..2='cd ../..'                            # Go 2 directories back.
alias ..3='cd ../../../'                        # Go 3 directories back.
alias ..4='cd ../../../..'                      # Go 4 directories back.
alias con='cd ~/.config'                        # Config folder.
alias disk='df -h'                              # Disk usage.
alias dou='yay -S'                              # yay install.
alias em='emacs'                                # Emacs editor.
alias esta='process_status'                     # Top process & status.
alias insta='sudo pacman -S'                    # pacman install.
alias inuse='sudo bandwhich'                    # Bandwidth usage.
alias jour='journalctl -p 3 -xb'                # Journalctl log.
alias kali='~/plugins/Kali/Kali -l'
alias nv='nvim'                                 # Nvim editor.
alias pacu='sudo pacman -Syyu'                  # pacman update.
alias pacuy='yes | pacu'                        # pacman update assuming yes to all.
alias qrest='qtile-cmd -o cmd -f restart'       # Restart Qtile
alias reflect='sudo reflector --sort rate -l 5 --save /etc/pacman.d/mirrorlist'      # Search the fastest mirrors.
alias remocac='sudo yay -Scc'                   # Clear pacman cache. 
alias remopac='sudo pacman -Rs'                 # Uninstall pacman package.
alias remounu='pacman -Qdtq | sudo remopac -'   # Uninstall unnused packages.
alias rr='ranger'                               # Ranger explorer.
alias see='bat'                                 # See a file content with Bat.
alias unlock='sudo rm /var/lib/pacman/db.lck'   # rm pacman lock.
alias vi='vim'                                  # Vim editor.
alias yup='yay -Sua'                            # yay update.

## Colorized elemetns list with 'exa'.
alias ll='lsd -alhF --group-dirs first' # List form of Elements.
alias ls='lsd'  # Dirty form of Elements.

alias alist='alias_list'                        # Aliases list.

alias sdgui='export HSA_OVERRIDE_GFX_VERSION=10.3.0; cd ~/lab/stable-diffusion-webui; python launch.py --skip-torch-cuda-test --disable-safe-unpickle --token-merging'

alias loradir='cd ~/lab/stable-diffusion-webui/models/Lora/'
alias sddir='cd ~/lab/stable-diffusion-webui/models/Stable-diffusion'

alias vcc='cd ~/lab/voice-changer/server/; source ../venv/bin/activate.fish;pactl load-module module-virtual-sink sink_name=cepe; python3 MMVCServerSIO.py -p 18888 --https true \
        --content_vec_500 pretrain/checkpoint_best_legacy_500.pt  \
        --hubert_base pretrain/hubert_base.pt \
        --hubert_soft pretrain/hubert/hubert-soft-0d54a1f4.pt \
        --nsf_hifigan pretrain/nsf_hifigan/model \
        --hubert_base_jp pretrain/rinna_hubert_base_jp.pt \
        --model_dir model_dir'

alias gs='cd ~/lab/gamesneeze/; ./toolbox.sh -l'

alias rvc='cd ~/lab/Retrieval-based-Voice-Conversion-WebUI/; source venv/bin/activate.fish; python infer-web.py'

import os
import re
import socket
import subprocess

from typing import List  # noqa: F401
from libqtile import bar, layout, widget, hook, qtile
from libqtile.command import lazy
from libqtile.config import Screen, Click, Match

from extra import keys, mouse, groups

### Colors
color = [
    ["#363635", "#363635"], # 0 Background
    ["#68786F", "#68786F"], # 1 Inactive
    ["#FF713D", "#FF713D"], # 2 Acent
    ["#2CD473", "#2CD473"], # 3 Primary
    ["#4581F0", "#4581F0"], # 4 Secondary
    ["#FFFDF7", "#FFFDF7"]  # 5 Font text
]

### Layouts
layout_style = {
    'border_focus': 'f0803c',
    'border_normal': '011627',
    'border_width': 3,
    'margin': 6
}

layouts = [
    layout.MonadTall(**layout_style),
    layout.Matrix(**layout_style),
    layout.Max(**layout_style),
    layout.TreeTab(
        active_bg = color[2],
        bg_color = color[0],
        font = 'Hack NF bold',
        fontsize = 11,
        inactive_bg = color[1],
        inactive_fg = color[5],
        padding = 16,
        padding_x = -2,
        padding_y = 20,
        sections = ["Main", "Back"],
        vspace = 3,
        **layout_style
    ),
]

### Widgets config
widget_defaults = dict(
    background = color[0],
    font = 'Hack NF bold',
    fontsize = 11,
    foreground = color[5],
    padding = 3,
)
extension_defaults = widget_defaults.copy()

### All glyps as '' are in https://www.nerdfonts.com/cheat-sheet

"""
battery = dict(
    background = color[3],
    charge_char = ' ',
    discharge_char = ' ',
    empty_char = ' ',
    fontsize = 13,
    format = '{char}{percent: 1.0%}',
    full_char = ' ',
    notify_below = True,
    low_percentage = 0.25,
    margin = 2,
    padding = 2,
    show_short_text = False,
    unknow_char = ' ',
    update_interval = 15
)
"""
caps = dict(
    background = color[1],
    fontsize = 25,
    padding = 4,
    update_interval = 0.5,
)

clock = dict(
    format = '%d/%m %a | %H:%M:%S',
    background = color[3],
    mouse_callbacks = {"Button1": lambda: qtile.cmd_spawn("galendae")}
)

cpu = dict(
    background = color[4],
    format = '{freq_current} GHz {load_percent}%'

)

current_layout = dict(
    background = color[4],
)

group_box = dict(
    active = color[5],
    disable_drag = True,
    fontsize = 28,
    highlight_method = "block",
    inactive = color[1],
    margin_x = -1,
    padding = 8,
    padding_y = 5,
    rounded = False,
    spacing = None,
    this_current_screen_border = color[3],
)

quick_exit = dict(
    background = "#D60000",
    countdown_start = 3,
    countdown_format = '{}s ',
    default_text = "  "
)

net = dict(
    background = color[2],
    foreground = '000000',
    format = ' {down}     {up} ',
    use_bits = False,
    padding = 1
)

sep = dict(
    linewidth = 12,
    foreground = color[0]
)

systray = dict(
    background = color[1],
    icon_size = 19,
    padding = 8
)

thermal_sensor = dict(
    background = color[4],
    metric = True,
    show_tag = False,
    threshold = 85
)

text_box = dict(
    text = "", # Icon: nf-oct-triangle_left
    padding = 0,
    fontsize = 23
)

wifi = dict(
    background = color[1],
    disconnected_message = '  ',
    fontsize = 20,
    foreground = color[5],
    format = '  '
)

# Background and Foreground def
def bg_fg(bg, fg):
    return {
        'background': bg,
        'foreground': fg
    }

### Screens
screens = [
    Screen(
        top = bar.Bar(
            [
                # Arch logo 
                widget.TextBox(
                                **bg_fg(color[3], color[5]),
                                fontsize = 20,
                                padding = -6,
                                text = "  "
                        ),
                widget.TextBox(
                                **bg_fg(color[4], color[3]),
                                padding = 0,
                                fontsize = 20,
                                text = ""
                        ),

                # Current layout name
                widget.CurrentLayout(**current_layout),
                widget.TextBox(**bg_fg(color[4], color[5]),
                               fontsize = 16,
                               text = " "
                        ),
                widget.GroupBox(**group_box),

                # Window name
                widget.Sep(**sep),
                widget.WindowName(font = 'Roboto bold'),
                widget.Sep(**sep),

                # Apps tray icons
                widget.TextBox(**bg_fg(color[0], color[1]), **text_box),
                widget.Systray(**systray),
                #widget.Wlan(**wifi), # Wifi icon
                widget.CapsNumLockIndicator(**caps), # Caps Indicator
                widget.Sep(**bg_fg(color[1], color[1]), text = " "),

                # Thermal sensor widget
                widget.TextBox(**bg_fg(color[1], color[4]), **text_box),
                #widget.TextBox(**bg_fg(color[4], color[5]), text = ""),
                #widget.ThermalSensor(**thermal_sensor),

                # CPU usage.
                widget.TextBox(**bg_fg(color[4], color[5]), text = "",
                               fontsize = 22),
                widget.CPU(**cpu),

                # Battery widget    
                #widget.TextBox(**bg_fg(color[4], color[3]), **text_box),
                #widget.Battery(**battery),

                # Brandwidth widget
                widget.TextBox(**bg_fg(color[4], color[2]), **text_box),
                widget.Net(**net),

                # Clock widget
                widget.TextBox(**bg_fg(color[2], color[3]), **text_box),
                widget.TextBox(**bg_fg(color[3], color[5]), text = "",
                               fontsize = 20),
                widget.Clock(**clock),

                # LogOut widget
                #widget.TextBox(**bg_fg(color[3], "#D60000"), **text_box),
                #widget.QuickExit(**quick_exit),
            ],
            24,
        ),
    ),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules = [
    # Run`xprop` to see the wm class
    {'wmclass': 'com.github.dahenson.agenda'},
    {'wmclass': 'confirm'},
    {'wmclass': 'Lollypop'},
    {'wmclass': 'Conky'},
    {'wmclass': 'telegram-desktop'},
    {'wmclass': 'copyq'},
    {'wmclass': 'dialog'},
    {'wmclass': 'error'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'Calculator.Skia.Gtk'},
    {'wmclass': 'Onboard'},
    {'wmclass': 'Galendae'},
    Match(title='New Page'),
    Match(title='Insert Symbol'),
    Match(title='File Upload'),
    Match(title='Picture in picture'),
    Match(title='Open files'),
    Match(title='Select File'),
    Match(title='Open Folder'),
    Match(title='Open File'),
])
auto_fullscreen = True
focus_on_window_activation = "smart"

### Run startup script
@hook.subscribe.startup_once
def startup_once():
    home = os.path.expanduser('~')
    subprocess.call([(home + '/.config/qtile/autostart.sh')])

wmname = "LG3D"

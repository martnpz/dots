--! Component provided by:
-- https://github.com/WillPower3309/awesome-dotfiles
--
--
--      ██╗   ██╗ ██████╗ ██╗     ██╗   ██╗███╗   ███╗███████╗
--      ██║   ██║██╔═══██╗██║     ██║   ██║████╗ ████║██╔════╝
--      ██║   ██║██║   ██║██║     ██║   ██║██╔████╔██║█████╗
--      ╚██╗ ██╔╝██║   ██║██║     ██║   ██║██║╚██╔╝██║██╔══╝
--       ╚████╔╝ ╚██████╔╝███████╗╚██████╔╝██║ ╚═╝ ██║███████╗
--        ╚═══╝   ╚═════╝ ╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝


-- ===================================================================
-- Initialization
-- ===================================================================

local wibox = require("wibox")
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

local os = os

local offsetx = dpi(64)
local offsety = dpi(300)
local screen = awful.screen.focused()
local icon_dir = string.format("%s/.config/awesome/images/",
                                os.getenv("HOME"))


awful.util.shell = "bash"

-- ===================================================================
-- Appearance & Functionality
-- ===================================================================

-- Bar color.
local bar_high = "linear:0,0:150.0:0,#F5CC00:40,#FF8D63"
local bar_normal ="linear:0,0:150.0:0,#9FE57A:30,#2CD473" 

local volume_icon = wibox.widget {
   widget = wibox.widget.imagebox
}

-- create the volume_adjust component
local volume_adjust = wibox({
   screen = awful.screen.focus(1),
   x = screen.geometry.width - offsetx,
   y = (screen.geometry.height / 2) - (offsety / 2),
   width = dpi(50),
   height = (offsety - 16),
   shape = function(cr, width, height)
      gears.shape.rounded_rect(cr, width, height, 26)
  end,
   visible = false,
   ontop = true,
   bg = "#36363590"
})

local volume_bar = wibox.widget{
   widget = wibox.widget.progressbar,
   shape = gears.shape.rounded_bar,
   background_color = "#86948C",
   max_value = 100,
   value = 0
}

volume_adjust:setup {
   layout = wibox.layout.align.vertical,
   {
      wibox.container.margin(
         volume_bar, dpi(14), dpi(20), dpi(20), dpi(20)
      ),
      forced_height = offsety * 0.75,
      direction = "east",
      layout = wibox.container.rotate
   },
   wibox.container.margin(
      volume_icon
   )
}

-- create a 4 second timer to hide the volume adjust
-- component whenever the timer is started
local hide_volume_adjust = gears.timer {
   timeout = 4,
   autostart = true,
   callback = function()
      volume_adjust.visible = false
   end
}

-- show volume-adjust when "volume_change" signal is emitted
awesome.connect_signal("volume_change",
   function()
      -- set new volume value
      -- Change "Mono" with your Master default output.
      awful.spawn.easy_async_with_shell(
        --"amixer sget Master | grep 'Mono:' | awk -F '[][]' '{print $2}'| sed 's/[^0-9]//g'",
        "pamixer --get-volume",
        function(stdout)
            local volume_level = tonumber(stdout) 
            volume_bar.value = volume_level
            if (volume_level > 1 and volume_level <= 85) then
                volume_icon:set_image(icon_dir.."normal.svg")
                volume_bar.color = bar_normal
           elseif (volume_level > 85) then
                volume_icon:set_image(icon_dir.."high.svg")
                volume_bar.color = bar_high
            else
                volume_icon:set_image(icon_dir.."mute.svg")
            end
         end,
         false
      )

      -- make volume_adjust component visible
      if volume_adjust.visible then
         hide_volume_adjust:again()
      else
         volume_adjust.visible = true
         hide_volume_adjust:start()
      end
   end
)

------------------------
-- Desktop Widgets.
------------------------

local wibox     = require("wibox")
local awful     = require("awful")
local gears     = require("gears")
local beautiful = require("beautiful")
local dpi       = beautiful.xresources.apply_dpi

local os = os

local offsetx = dpi(400)
local offsety = dpi(500)
local screen  = awful.screen.focused()

local images  = string.format("%s/.config/awesome/images/", os.getenv("HOME"))
local cover_cache = string.format("%s/.cache/covers/", os.getenv("HOME"))

awful.util.shell = "bash"

local cover = wibox.widget {
  widget = wibox.widget.imagebox,
  clip_shape = function(cr, width, height)
      gears.shape.rounded_rect(cr, width, height, 16)
  end,
}

local player = wibox({
    screen = screen,
    window = "casa",
    x = 130,
    y = 140,
    width = 260,
    height = 360,
    shape = function(cr, width, height)
      gears.shape.rounded_rect(cr, width, height, 16)
    end,
    visible = true,
    ontop = false,
    bg = "#36363599",
})

local song = wibox.widget{
  {
    widget = wibox.widget.textbox,
    font = "Poppins Bold 14",
    align = "center",
    valign = "center"
  },
  widget = wibox.container.background,
  fg = "#FFD812",
}

local artist = wibox.widget{
  {
    widget = wibox.widget.textbox,
    font = "Poppins 11",
    align = "center",
    valign = "center"
  },
  widget = wibox.container.background,
  fg = "#FFFDF7"
}

player:setup {
  layout = wibox.layout.align.vertical,
  {
    wibox.container.margin(
      cover, dpi(18), dpi(15), dpi(18), dpi(18)
    ),
    direction = "north",
    layout = wibox.container.rotate
  },
  wibox.container.margin(
    song, dpi(0), dpi(0), dpi(0), dpi(0)
  ),
  wibox.container.margin(
    artist, dpi(0), dpi(0), dpi(0), dpi(10)
  )
}

local cover_load = gears.timer {
  timeout = 15,
  call_now = true,
  autostart = true,
  callback = function()
    awful.spawn.easy_async_with_shell(
      "~/.config/awesome/scripts/getsong c",
      function()
          awful.spawn.easy_async_with_shell(
          "~/.config/awesome/scripts/getsong p",
          function(stdout)
            local song_path = tostring(stdout)
            if string.match(string.lower(song_path), "undefined") then
                cover:set_image(gears.surface.load_uncached(images .. 'cover.svg'))
            else
                cover:set_image(gears.surface.load_uncached(cover_cache .. song_path))
                cover:set_image(gears.surface.load_uncached(images .. 'cover.svg'))
            end
          end
          )
      end
    )
  end
}

local song_load = gears.timer {
  timeout = 2,
  autostart = true,
  callback = function()
    awful.spawn.easy_async_with_shell(
      "~/.config/awesome/scripts/getsong s",
      function(stdout)
        song.widget.text = stdout
      end
    )
    awful.spawn.easy_async_with_shell(
      "~/.config/awesome/scripts/getsong a",
      function(stdout)
        artist.widget.text = stdout
      end
    )
    end
}

----------
-- Quotes
----------

local quotes = wibox({
    screen = screen,
    tag = 5, 
    x = 130,
    y = 520,
    width = 260,
    height= 170,
    shape = function(cr, width, height)
      gears.shape.rounded_rect(cr, width, height, 16)
    end,
    visible = true,
    ontop = false,
    bg = "#36363599"
})

local quote_icon = wibox.widget{
  {
    widget = wibox.widget.textbox,
    font = "Hack Nerd Font Mono 28",
    align = "center",
    valign = "center",
    text = "ﱕ"
  },
  widget = wibox.container.background,
  fg = "#9FE57A"
}

local quote = wibox.widget{
  {
    widget = wibox.widget.textbox,
    font = "Poppins 11",
    align = "center",
    valign = "center"
  },
  widget = wibox.container.background,
  fg = "#FFFDF7",
}

local quote_autor = wibox.widget{
   {
    widget = wibox.widget.textbox,
    font = "Poppins Bold 10",
    align = "center",
    valign = "center",
  },
  widget = wibox.container.background,
  fg = "#9FE57A"
}

quotes:setup {
  layout = wibox.layout.align.vertical,
  {
    wibox.container.margin(
      quote_icon, dpi(0), dpi(0), dpi(0), dpi(0)
    ),
    direction = "north",
    layout = wibox.container.rotate
  },
  {
    wibox.container.margin(
      quote, dpi(5), dpi(5), dpi(0), dpi(0)
    ),
    direction = "north",
    layout = wibox.container.rotate
  },
  {
    wibox.container.margin(
      quote_autor, dpi(0), dpi(0), dpi(0), dpi(0)
    ),
    direction = "north",
    layout = wibox.container.rotate
  }
}

local quote_load = gears.timer {
  timeout = 3600,
  call_now = true,
  autostart = true,
  callback = function()
    awful.spawn.easy_async_with_shell(
      "~/.config/awesome/scripts/quotes",
      function(stdout)
        quote.widget.text = stdout:gsub("[|].*", "")
        quote_autor.widget.text = stdout:gsub(".*[|]", "")
      end
    )
  end
}

----------
-- System
----------

local system = wibox({
    screen = screen,
    x = 130,
    y = 710,
    width = 260,
    height= 140,
    shape = function(cr, width, height)
      gears.shape.rounded_rect(cr, width, height, 16)
    end,
    visible = true,
    ontop = false,
    bg = "#36363599"
})

local cpu_usage = wibox.widget {
  {
    {
      {
        widget = wibox.widget.textbox,
        text = "",
        font = "Hack Nerd Font Mono 35"
      },
      widget = wibox.container.background,
      fg = "#FF713D"
    },
    widget = wibox.container.margin,
    left = 33
  },
	forced_width = dpi(106),
	forced_height = dpi(75),
	min_value = 0,
	max_value = 100,
	color = "#FF713D",
	border_color = "#363635",
	border_width = dpi(16),
	widget = wibox.container.radialprogressbar
}

local ram_usage = wibox.widget {
  {
    {
      {
        widget = wibox.widget.textbox,
        text = "",
        font = "Hack Nerd Font Mono 35"
      },
      widget = wibox.container.background,
      fg = "#2CD473"
    },
    widget = wibox.container.margin,
    left = 33
  },
	forced_width = dpi(52),
	forced_height = dpi(75),
	padding = 0,
	min_value = 0,
	max_value = 100,
	color = "#2CD473",
	border_color = "#363635",
	border_width = dpi(16),
	widget = wibox.container.radialprogressbar
}

system:setup {
  layout = wibox.layout.align.horizontal,
  {
    wibox.container.margin(
      cpu_usage, dpi(5), dpi(10), dpi(12), dpi(12)
    ),
    direction = "south",
    layout = wibox.container.rotate
  },
  {
    wibox.container.margin(
      ram_usage, dpi(10), dpi(5), dpi(12), dpi(12)
    ),
    direction = "south",
    layout = wibox.container.rotate
  },
}

local system_load = gears.timer {
  timeout = 2,
  call_now = true,
  autostart = true,
  callback = function()
    awful.spawn.easy_async_with_shell(
      "~/.config/awesome/scripts/getcpu",
      function(stdout)
        cpu_usage.value = tonumber(stdout)
      end
    )
    awful.spawn.easy_async_with_shell(
      "~/.config/awesome/scripts/getram",
      function(stdout)
        ram_usage.value = tonumber(stdout)
      end
    )
  end
}

----------
-- Disks
----------

local disks = wibox({
    screen = screen,
    x = 130,
    y = 870,
    width = 260,
    height= 100,
    shape = function(cr, width, height)
      gears.shape.rounded_rect(cr, width, height, 16)
    end,
    visible = true,
    ontop = false,
    bg = "#36363599"
})

local disk_one = wibox.widget {
  {
    valign = "center",
    font   = "Poppins Bold 12",
    widget = wibox.widget.textbox,
    text   = "\t\t\t sda3",
  },
    max_value     = 100,
    forced_height = 27,
    forced_width  = 100,
    value         = 0,
    border_width  = 0,
    shape         = gears.shape.rounded_bar,
    color         = "#5A92FA",
    background_color = "#363635",
    widget        = wibox.widget.progressbar,
}

local disk_two = wibox.widget {
    {
      text   = "\t\t\t sdb1",
      valign = "center",
      halign = "center",
      font   = "Poppins Bold 12",
      widget = wibox.widget.textbox,
    },
        max_value     = 100,
        forced_height = 10,
        forced_width  = 100,
        value         = 0,
        border_width  = 0,
        shape         = gears.shape.rounded_bar,
        color         = "#5A92FA",
        background_color = "#363635",
        widget        = wibox.widget.progressbar,
}

disks:setup {
  layout = wibox.layout.align.vertical,
  {
    wibox.container.margin(
      disk_one, dpi(12), dpi(12), dpi(14), dpi(14)
    ),
    direction = "north",
    layout = wibox.container.rotate
  },
  {
    wibox.container.margin(
      disk_two, dpi(12), dpi(12), dpi(0), dpi(14)
    ),
    direction = "north",
    layout = wibox.container.rotate
  },
}

local disks_load = gears.timer {
  timeout = 1800,
  autostart = true,
  call_now = true,
  callback = function()
    awful.spawn.easy_async_with_shell(
      "~/.config/awesome/scripts/getdisk a",
      function(stdout)
        disk_one.value = tonumber(stdout)
      end
    )
    awful.spawn.easy_async_with_shell(
      "~/.config/awesome/scripts/getdisk b",
      function(stdout)
        disk_two.value = tonumber(stdout)
      end
    )
  end
}

-- Close widgets.
awesome.connect_signal("wi",
                       function()
                         player.visible = false
                         quotes.visible = false
                       end
)

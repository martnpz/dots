---------------------------
-- Leaf
-- martnpz awesome theme.
---------------------------

-- Requires.
local gears        = require("gears")
local theme_assets = require("beautiful.theme_assets")

-- Theme preferences.
local theme = {}
local color = {
  "#484444", --  1 Background.
  "#706A68", --  2 Background bright.
  "#FFAA6C", --  3 Red.
  "#FFB987", --  4 Red bright.
  "#77E8A7", --  5 Primary.
  "#80E4AA", --  6 Primary bright
  "#FDDB8C", --  7 Alert.
  "#F7DEA3", --  8 ALert bright.
  "#A0E57A", --  9 Secondary
  "#ADE590", -- 10 Secondary bright.
  "#B8BAFF", -- 11 Tertiary:w
  "#C1C4FB", -- 12 Tertiary bright.
  "#A9DEE5", -- 13 Cyan.
  "#C0E1E5", -- 14 Cyan bright.
  "#FCFCFC", -- 15 Foreground.
  "#EFEFEF", -- 16 Foreground bright.
}

theme.dir       = "~/.config/awesome/themes/wind/"
theme.font      = "Hack wNerd Font Bold 10"

-- Corners.
theme.corner_radius = 16

-- Global color.
theme.color_bg          = color[1]
theme.color_bg_b        = color[2]
theme.color_red         = color[3]
theme.color_red_b       = color[4]
theme.color_primary     = color[5]
theme.color_primary_b   = color[6]
theme.color_alert       = color[7]
theme.color_alert_b     = color[8]
theme.color_secondary   = color[9]
theme.color_secondary_b = color[10]
theme.color_tertiary    = color[11]
theme.color_tertiary_b  = color[12]
theme.color_cyan        = color[13]
theme.color_cyan_b      = color[14]
theme.color_fg          = color[15]
theme.color_fg_b        = color[16]

-- Task Background.
--theme.bg_focus    = "linear:-4,2:300,0:0,#2CD473:10,#9FE57A"
theme.bg_focus    = color[1]
theme.bg_minimize = color[2]
theme.bg_normal   = color[1]
theme.bg_urgent   = color[3]

-- Task Foreground.
theme.fg_focus    = color[15]
theme.fg_minimize = color[16]
theme.fg_normal   = color[15]
theme.fg_urgent   = color[1]

-- Border.
theme.border_focus  = color[5]
theme.border_marked = color[6]
theme.border_normal = color[1]
theme.border_width  = 3

-- Gaps.
theme.useless_gap       = 5
theme.gap_single_client = true

-- Tags.
theme.taglist_bg_empty    = color[1]
theme.taglist_bg_focus    = color[1]
theme.taglist_bg_occupied = color[1]
theme.taglist_bg_urgent   = color[1]
theme.taglist_bg_volatile = color[1]

theme.taglist_fg_focus    = color[5]
theme.taglist_fg_urgent   = color[3]
theme.taglist_fg_occupied = color[16]
theme.taglist_fg_empty    = "#a2a2a2"
theme.taglist_fg_volatile = color[2]

theme.taglist_font        = "Hack Nerd Font Mono 16"
theme.taglist_spacing     = 0

-- Task list.
theme.tasklist_align        = "center"
theme.tasklist_font         = "RobotoMono Nerd Font Bold 11"
theme.tasklist_disable_icon = true

-- Wibar.
theme.wibar_border_width = 3

-- Panel.
theme.left_panel_width  = 100
theme.left_panel_height = 20

-- Tray.

theme.bg_systray           = color[2]
theme.systray_icon_spacing = 6

-- Hotkeys popup.
theme.hotkeys_bg               = color[1] .. "90"
theme.hotkeys_fg               = color[16]
theme.hotkeys_border_width     = 4
theme.hotkeys_border_color     = color[7]
theme.hotkeys_shape            = function (cr, width, height)
    gears.shape.rounded_rect(cr, width, height, 16)
end
theme.hotkeys_modifiers_fg     = color[2]
theme.hotkeys_label_bg         = color[9]
theme.hotkeys_label_fg         = color[15]
theme.hotkeys_font             = "Hack Nerd Font Bold 11"
theme.hotkeys_description_font = "Hack Nerd Font 11"
theme.hotkeys_margin           = 6
theme.hotkeys_width            = 10

--- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Layout Icons.
theme.layout_fairv    = theme.dir .. "icons/fairh.svg"
theme.layout_floating = theme.dir .. "icons/floating.svg"
theme.layout_max      = theme.dir .. "icons/max.svg"
theme.layout_tile     = theme.dir .. "icons/tile.svg"
theme.layout_tiletop  = theme.dir .. "icons/tiletop.svg"

theme.icon_theme = nil

return theme

---------------------------
-- Leaf
-- martnpz awesome theme.
---------------------------

-- Requires.
local gears        = require("gears")
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local xrdb = xresources.get_current_theme()

-- Theme preferences.
local theme = {}

theme.dir       = "~/.config/awesome/themes/wind/"
theme.font      = "Hack Nerd Font Mono Bold 10"

-- Corners.
theme.corner_radius = 16

-- Global color.
theme.color_bg          = xrdb.color0
theme.color_bg_b        = xrdb.color8
theme.color_red         = xrdb.color1
theme.color_red_b       = xrdb.color9
theme.color_primary     = xrdb.color2
theme.color_primary_b   = xrdb.color10
theme.color_alert       = xrdb.color3
theme.color_alert_b     = xrdb.color11
theme.color_secondary   = xrdb.color4
theme.color_secondary_b = xrdb.color12
theme.color_tertiary    = xrdb.color5
theme.color_tertiary_b  = xrdb.color13
theme.color_cyan        = xrdb.color6
theme.color_cyan_b      = xrdb.color14
theme.color_fg          = xrdb.color7
theme.color_fg_b        = xrdb.color15

-- Task Background.
--theme.bg_focus    = "linear:-4,2:300,0:0,#2CD473:10,#9FE57A"
theme.bg_focus    = xrdb.color0
theme.bg_minimize = xrdb.color8
theme.bg_normal   = xrdb.color0
theme.bg_urgent   = xrdb.color1

-- Task Foreground.
theme.fg_focus    = xrdb.color7
theme.fg_minimize = xrdb.color15
theme.fg_normal   = xrdb.color7
theme.fg_urgent   = xrdb.color0

-- Border.
theme.border_focus  = xrdb.color2
theme.border_marked = xrdb.color10
theme.border_normal = xrdb.color0
theme.border_width  = 3

-- Gaps.
theme.useless_gap       = 5
theme.gap_single_client = true

-- Tags.
theme.taglist_bg_empty    = xrdb.color0
theme.taglist_bg_focus    = xrdb.color0
theme.taglist_bg_occupied = xrdb.color0
theme.taglist_bg_urgent   = xrdb.color0
theme.taglist_bg_volatile = xrdb.color0

theme.taglist_fg_focus    = xrdb.color2
theme.taglist_fg_urgent   = xrdb.color1
theme.taglist_fg_occupied = xrdb.color15
theme.taglist_fg_empty    = "#a2a2a2"
theme.taglist_fg_volatile = xrdb.color8

theme.taglist_font        = "Hack Nerd Font Mono 16"
theme.taglist_spacing     = 0

-- Task list.
theme.tasklist_align        = "center"
theme.tasklist_font         = "RobotoMono Nerd Font Bold 11"
theme.tasklist_disable_icon = true

-- Wibar.
theme.wibar_border_width = 3

-- Panel.
theme.left_panel_width  = 100
theme.left_panel_height = 20

-- Tray.

theme.bg_systray           = xrdb.color8
theme.systray_icon_spacing = 6

-- Hotkeys popup.
theme.hotkeys_bg               = xrdb.color0 .. "90"
theme.hotkeys_fg               = xrdb.color15
theme.hotkeys_border_width     = 4
theme.hotkeys_border_color     = xrdb.color3
theme.hotkeys_shape            = function (cr, width, height)
    gears.shape.rounded_rect(cr, width, height, 16)
end
theme.hotkeys_modifiers_fg     = xrdb.color8
theme.hotkeys_label_bg         = xrdb.color4
theme.hotkeys_label_fg         = xrdb.color7
theme.hotkeys_font             = "Hack Nerd Font Bold 11"
theme.hotkeys_description_font = "Hack Nerd Font 11"
theme.hotkeys_margin           = 6
theme.hotkeys_width            = 10

--- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Layout Icons.
theme.layout_fairv    = theme.dir .. "icons/fairh.svg"
theme.layout_floating = theme.dir .. "icons/floating.svg"
theme.layout_max      = theme.dir .. "icons/max.svg"
theme.layout_tile     = theme.dir .. "icons/tile.svg"
theme.layout_tiletop  = theme.dir .. "icons/tiletop.svg"

theme.icon_theme = nil

return theme

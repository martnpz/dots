-- If LuaRocks is installed, make sure that packages installed through it are
pcall(require, "luarocks.loader")

-- Standard awesome library.
local gears = require("gears") -- Utils for color parsing.
local awful = require("awful") -- Window managment.
              require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

local _dbus = dbus; dbus = nil
local naughty = require("naughty")
dbus = _dbus

-- Notification library
--local naughty = require("naughty")
local hotkeys_popup = require("awful.hotkeys_popup")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Extensions.
require("extensions.volume-adjust")
--require("extensions.deskwidgets") -- Disabled for double monitor set-up.
local vicious  = require("vicious")
local calendar = require("extensions.calendar")
awful.util.shell = "bash"


-- Error handling
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

--naughty.connect_signal("request::display", function() end)

-- Themes path.
local themes = {"leaf", "wind", "xresources","default"}
local select_theme = themes[1]
local theme_dir = string.format("%s/.config/awesome/themes/%s/theme.lua",
                                os.getenv("HOME"), select_theme)
beautiful.init(theme_dir)
beautiful.init(string.format(gears.filesystem.get_configuration_dir() ..
                             "/themes/%s/theme.lua", select_theme))

-- Autostart.
awful.spawn(string.format("deadd-notification-center"))
awful.spawn(string.format("picom --config %s/.config/picom/awesome_picom.conf --experimental-backends", os.getenv("HOME")))
--awful.spawn(string.format("aw-discord"))

-- On the fly useless gaps change.
function gaps_resize(thatmuch, s, t)
    local scr = s or awful.screen.focused()
    local tag = t or scr.selected_tag
    tag.gap = tag.gap + tonumber(thatmuch)
    awful.layout.arrange(scr)
end

-- This is used later as the default terminal and editor to run.
terminal = "alacritty"
editor =  "emacs" or os.getenv("EDITOR")
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Valid modifiers: Mod1, Mod2, Mod3, Mod4, Mod5, Shift, Lock and Control.
local modkey  = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.fair,
    awful.layout.suit.tile.top,
    awful.layout.suit.floating,
    awful.layout.suit.max,
}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

awful.screen.connect_for_each_screen(function(s)
    -- Each screen has its own tag table.
    awful.tag({ " ", " ", "󱊹 ", " ", "󱕅 ", ""}, s, awful.layout.layouts[1])

    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))

    --{{ Widgets config.
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons,
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen   = s,
        filter   = awful.widget.tasklist.filter.focused,
        style    = {
            shape = gears.shape.rounded_bar
        },
}

    -- Wibox.
    s.mywibox = awful.wibar({
            position = "top",
            screen = s,
            height = 22,
            bg = beautiful.bg_normal .. "00"
    })

    -- Local widgets.
    function corner (direction, color)
        local shape = ""
        if direction == "right" then shape = ""
        elseif direction == "left" then shape = ""
        end

            corner_widget = wibox.widget {
                {
                    text = shape,
                    font = "Hack Nerd Font 13",
                    widget = wibox.widget.textbox
                },
                bg = beautiful.color_bg .. "00",
                fg = color,
                widget = wibox.container.background
            }
        return corner_widget
    end

    local center_sep = wibox.widget {
        widget = wibox.widget.separator,
        forced_width = 140,
        color = "#00000000"
    }

    local sep = wibox.widget {
        widget = wibox.widget.separator,
        forced_width = 10,
        color = "00000000"
    }

    local my_cpu   = wibox.widget.textbox()
    local my_cpu_f = wibox.widget.textbox()
    local my_cpu_t = awful.tooltip({ objects = { my_cpu.widget },})
    vicious.register(my_cpu, vicious.widgets.cpu,
                     function (widget, args)
                         return ' <big></big> ' .. args[1] .. '% '
                     end, 5)

    my_cpu = wibox.widget {
        my_cpu,
        fg = beautiful.color_bg,
        bg = beautiful.color_alert,
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background
    }

    local my_ram   = wibox.widget.textbox()
    local my_ram_t = awful.tooltip({objects = {my_ram.widget},})
    vicious.cache(vicious.widgets.mem)
    vicious.register(my_ram, vicious.widgets.mem,
                     function (widget, args)
                         return '   ' .. (args[2] / 1000) .. ' GB '
                     end, 10)

    my_ram = wibox.widget {
        my_ram,
        fg = beautiful.color_bg,
        bg = beautiful.color_primary_b,
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background,

    }
   local mouse_battery = wibox.widget {
        {
            text = '',
            widget = wibox.widget.textbox,
        },
        bg         = beautiful.color_cyan,
        fg         = beautiful.color_fg,
        shape      = gears.shape.rounded_rect,
        shape_clip = true,
        widget     = wibox.container.background,
    }

   -- Reload mouse battery percent.
   gears.timer {
        timeout   = 2,
        call_now  = true,
        autostart = true,
        callback  = function()
            awful.spawn.easy_async_with_shell(
                "~/.config/awesome/scripts/getmouse",
                function(stdout)
                    if tonumber(stdout) == 0 then
                        mouse_battery.widget.forced_width = 0
                    else
                        mouse_battery.widget.text = string.format(" 󰦋 %s", stdout)
                        mouse_battery.widget.forced_width = 55
                    end
                end
            )
        end
    }

--[[ -- Switch keyboard layout between 'latam' and 'Eu'.
 --     local keyboard_layout = wibox.widget {
 --       {
 --           text = 'aw',
 --           widget = wibox.widget.textbox,
 --       },
 --
 --       bg         = "linear:2,1:50.0:0,#35D6F2:10,#4581F0",
 --       fg         = beautiful.color_fg,
 --       shape      = gears.shape.rounded_rect,
 --       shape_clip = true,
 --       widget     = wibox.container.background,
 --   }
 --
 --   gears.timer {
 --       timeout   = 10000,
 --       call_now  = true,
 --       autostart = true,
 --       callback  = function()
 --            awful.spawn.easy_async_with_shell(
 --               "~/.config/awesome/scripts/kblayout l",
 --                   function(stdout)
 --                   keyboard_layout.widget.text = tostring(stdout)
 --               end
 --           )
 --           end
 --   }

 --   keyboard_layout:connect_signal("button::press", function(_, _, _, button)
 --       if button == 1 then
 --           awful.spawn.easy_async_with_shell(
 --               "~/.config/awesome/scripts/kblayout s",
 --               function(stdout)
 --                   keyboard_layout.widget.text = tostring(stdout)
 --               end
 --           )
 --       end
 --   end)
 --]]


  --[[
  -- Switch microphone mute.
      local toggle_mute = wibox.widget {
        {
            text = 'aw',
            widget = wibox.widget.textbox,
        },
 
        bg         = "linear:2,1:50.0:0,#484444:10,#706A68",
        fg         = beautiful.color_fg,
        shape      = gears.shape.rounded_rect,
        shape_clip = true,
        widget     = wibox.container.background,
    }
 
    gears.timer {
        timeout   = 4,
        call_now  = true,
        autostart = true,
        callback  = function()
             awful.spawn.easy_async_with_shell(
                "~/.config/awesome/scripts/toggle_mic s",
                    function(stdout)
                    toggle_mute.widget.text = tostring(stdout)
                end
            )
            end
    }

    toggle_mute:connect_signal("button::press", function(_, _, _, button)
        if button == 1 then
            awful.spawn.easy_async_with_shell(
                "~/.config/awesome/scripts/toggle_mic t",
                function(stdout)
                    toggle_mute.widget.text = tostring(stdout)
                end
            )
        end
    end)
 
--]]

    -- Applications trayer.
    local my_systray = wibox.widget {
      screen = "primary",
        {
            wibox.widget.systray(),
            left   = 10,
            top    = 2,
            bottom = 2,
            right  = 10,
            widget = wibox.container.margin,
        },
        bg         = beautiful.bg_systray,
        shape      = gears.shape.rounded_rect,
        shape_clip = true,
        widget     = wibox.container.background,
    }

    local eths = { 'enp12s0', 'wlp0s20f0u1' }
    local my_net_up = wibox.widget.textbox()
    local my_net_down = wibox.widget.textbox()
    vicious.register( my_net_down, vicious.widgets.net,
                      function(widget,args)
                          local i_down = ''
                          for i = 1, #eths do
                              local e = eths[i]
                              if args["{"..e.." carrier}"] == 1 then
                                  if e == 'wlp0s20u1' then
                                      i_down = i_down ..' '..'<big>󰓅</big> '..args['{'..e..' down_kb}']..' KB  '
                                  else
                                      i_down = i_down ..' '..'<big>󰓅</big> '..args['{'..e..' down_kb}']..' KB  '
                                  end
                              end
                          end

                          return i_down
                      end, 1 )

    vicious.register( my_net_up, vicious.widgets.net_up,
                      function(widget,args)
                          i_up = ''
                          for i = 1, #eths do
                              a = eths[i]
                              if args["{"..a.." carrier}"] == 1 then
                                  if a == 'wlp0s20u4' then
                                      i_up = ' ' .. i_up .. args['{'..a..' up_kb}']..' KB  '
                                  else
                                      i_up = ' ' .. i_up .. args['{'..a..' up_kb}']..' KB  '
                                  end
                              end
                          end

                          return i_up
                      end, 1 )

    my_net_up = wibox.widget {
        my_net_up,
        bg = beautiful.color_tertiary,
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background,
    }

    my_net_down = wibox.widget {
        my_net_down,
        bg = beautiful.color_secondary,
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background,
    }

    local my_date = wibox.widget {
        {
            {
                format= "󰨳 %d/%m",
                refresh = 1,
                widget = wibox.widget.textclock,
            },
            left   = 10,
            right  = 10,
            widget = wibox.container.margin
        },
        bg         = beautiful.color_red,
        fg         = beautiful.color_bg,
        shape      = gears.shape.rounded_rect,
        shape_clip = true,
        widget     = wibox.container.background,
    }

    local cw = calendar({
            placement = 'top_right',
            radius = 16,
    })

    my_date:connect_signal("button::press", function(_, _, _, button)
        if button == 1 then cw.toggle() end
    end)

    local my_clock = wibox.widget {
        {
            {
                format = " %T",
                refresh = 1,
                widget = wibox.widget.textclock,
            },
            left   = 5,
            right  = 10,
            widget = wibox.container.margin,
        },
        bg        = beautiful.color_alert,
        fg        = beautiful.color_bg,
        shape     = gears.shape.rounded_rect,
        widget    = wibox.container.background,
    }

    -- Add widgets to the bar.
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        -- Left widgets
        {
            layout = wibox.layout.fixed.horizontal,

            corner("left", beautiful.color_bg),
            s.mytaglist,
            corner("right", beautiful.color_bg),

            sep,
            corner("left", beautiful.color_tertiary),
            s.mylayoutbox,
            corner("right", beautiful.color_tertiary),
            sep,
            my_cpu,
            sep,
            my_ram,
            sep,
            mouse_battery,

            center_sep,
        },

        -- Middle widget
        s.mytasklist,

        -- Right widgets
        {
            center_sep,
            layout = wibox.layout.fixed.horizontal,
            toggle_mute,
            sep,
            my_systray,
            sep,
            keyboard_layout,
            sep,
            my_net_down,
            sep,
            my_net_up,
            sep,
            my_date,
            sep,
            my_clock,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
--root.buttons(gears.table.join(
--    awful.button({ }, 9, awful.spawn(terminal)),
--    awful.button({ }, 5, awful.tag.viewprev)
--))
--- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey,           }, "j",      hotkeys_popup.show_help,
              {description="Show keybinding list.", group="awesome"}),

    awful.key({ modkey, "Shift"   }, "Home",
        function ()
            awful.spawn.easy_async_with_shell("xrdb ~/.Xresources")
            awful.spawn.easy_async_with_shell("alacritty_theme")
            awesome.restart()
        end,
              {description = "Reload awesome.", group = "awesome"}),

    awful.key({ modkey,           }, "Right",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "Focus next window.", group = "client"}
    ),

    awful.key({ modkey,           }, "Left",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "Focus previous window.", group = "client"}
    ),

        awful.key({ modkey,           }, "space",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "Focus next window.", group = "client"}
    ),

    awful.key({ modkey,}, "h", function () awful.screen.focus_relative(1) end,
              {description = "Switch between monitors", group = "client"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "Down", function () awful.client.swap.byidx(  1)    end,
              {description = "Swap with next window.", group = "client"}),

    awful.key({ modkey, "Shift"   }, "Up", function () awful.client.swap.byidx( -1)    end,
              {description = "Swap with previous window.", group = "client"}),

    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),

    -- Launch.
    awful.key({ modkey, "Shift"   }, "n", function () awful.spawn.easy_async_with_shell("cd ~/plugins/Kali/; ./Kali -l") end,
              {description = "Load Kali for CS:GO.", group = "launcher"}),

    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "Open a terminal.", group = "launcher"}),

    awful.key({ modkey,           }, "r", function () awful.spawn("rofi -show drun -theme style_6") end,
              {description = "Open Apprications menu.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "p", function () awful.spawn("power") end,
              {description = "Open power menu.", group = "launcher"}),

    awful.key({ modkey, "Control" }, "r", function () awful.spawn("rofi -show") end,
              {description = "Open Application in window.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "s", function () awful.spawn("flameshot gui") end,
              {description = "Take screenshot.", group = "launcher"}),

    awful.key({ modkey,           }, "v", function () awful.spawn("copyq show") end,
              {description = "Open clipboard.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "b", function () awful.spawn(terminal .. " -e btop") end,
              {description = "Open task manager.", group = "launcher"}),

    awful.key({ modkey,           }, "n", function () awful.spawn("notification_center") end,
              {description = "Open notification center.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "q", function () awful.spawn("deepin-calculator") end,
              {description = "Open advanced calculator.", group = "launcher"}),

    awful.key({ modkey,           }, "q", function () awful.spawn("calc_prompt") end,
              {description = "Open calculator.", group = "launcher"}),

    awful.key({ modkey,           }, "c", function () awful.spawn("telegram-desktop") end,
              {description = "Open telegram.", group = "launcher"}),

    awful.key({ modkey,           }, "d", function () awful.spawn("pkill -USR1 '^redshift$'") end,
              {description = "Start/Stop redshift.", group = "launcher"}),

    awful.key({ modkey,           }, "period", function () awful.spawn("copymoji") end,
              {description = "Open emoji menu.", group = "launcher"}),

    awful.key({ modkey,           }, "t", function () awful.spawn("thunar") end,
              {description = "Open explorer.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "t", function () awful.spawn("spotify") end,
              {description = "Open Spotify.", group = "launcher"}),

    awful.key({ modkey,           }, "b", function () awful.spawn("pavucontrol") end,
              {description = "Open audio control.", group = "launcher"}),

    awful.key({ modkey,           }, "g", function () awful.spawn("steam") end,
              {description = "Open Steam.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "g", function () awful.spawn(editor) end,
              {description = "Open Emacs.", group = "launcher"}),

    awful.key({ modkey,           }, "b", function () awful.spawn("fastsurf") end,
              {description = "Open surf.", group = "launcher"}),

    awful.key({ modkey,           }, "k", function () awful.spawn("killapp") end,
              {description = "Open kill menu.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "h", function () awful.spawn("xkill") end,
              {description = "Open xkill.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "c", function () awful.spawn("copycolor") end,
              {description = "Open color picker.", group = "launcher"}),

    awful.key({ modkey,           }, "i", function () awful.spawn("xterm -b 30 -e cava") end,
              {description = "Open cava visualizer.", group = "launcher"}),

    awful.key({ modkey,           }, "s", function () awful.spawn("google-chrome-stable") end,
              {description = "Open Chrome Browser.", group = "launcher"}),

    awful.key({ modkey,           }, "w", function () awful.spawn("discord") end,
              {description = "Open Discord.", group = "launcher"}),

    awful.key({ modkey, "Shift"   }, "w", function () awful.spawn("toggle_mic t") end,
              {description = "Toggle mute/unmute microphone.", group = "launcher"}),

    -- Remap of keys for each application.
    -- Geometry dash.
  --[[
    awful.key({ "Control",        }, "h", function () 

      if awful.rules.match(client.focus, { name = "Geometry Dash" }) then
          
          awful.spawn("notify-send cp")
          awful.spawn("xdotool key d")
      end
    end,
              {description = client_name, group = "launcher"}),
  --]]
    -- Function key.
    awful.key({}, "XF86AudioRaiseVolume",
        function ()
            awful.spawn("pamixer -i 5", false)
            awesome.emit_signal("volume_change")
        end,
              {description = "Increase volume.", group = "Hotkeys"}),

    awful.key({}, "XF86AudioLowerVolume",
        function ()
            awful.spawn("pamixer -d 5", false)
            awesome.emit_signal("volume_change")
        end,
              {description = "Reduce volume.", group = "Hotkeys"}),

    awful.key({}, "XF86AudioMute",
        function ()
            awful.spawn("pamixer -t", false)
            awesome.emit_signal("volume_change")
        end,
              {description = "Mute volume.", group = "Hotkeys"}),

   awful.key({}, "XF86MonBrightnessUp", function() awful.spawn("xbacklight -inc 10", false)   end,
              {description = "Increase brightness.", group = "Hotkeys"}),

   awful.key({}, "XF86MonBrightnessDown", function() awful.spawn("xbacklight -dec 10", false) end,
              {description = "Decrease brightness.", group = "Hotkeys"}),

   awful.key({}, "XF86AudioPlay", function() awful.spawn("playerctl play-pause", false)       end,
              {description = "PLay/Pause multimedia.", group = "Hotkeys"}),

   awful.key({}, "XF86AudioPrev", function() awful.spawn("playerctl previous", false)         end,
              {description = "Previous song.", group = "Hotkeys"}),

   awful.key({}, "XF86AudioNext", function() awful.spawn("playerctl next", false)             end,
              {description = "Next song.", group = "Hotkeys"}),

    awful.key({ modkey, "Shift"   }, "Right",     function () awful.tag.incmwfact( 0.05)      end,
              {description = "Increase master window.", group = "layout"}),

    awful.key({ modkey, "Shift"   }, "Left",     function () awful.tag.incmwfact(-0.05)       end,
              {description = "Decrease master window", group = "layout"}),

    awful.key({ modkey, "Shift"   }, "z",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "Increase the number of master clients.", group = "layout"}),

    awful.key({ modkey,           }, "z",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "Decrease the number of master clients.", group = "layout"}),

    awful.key({ modkey, "Control" }, "z",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "Increase the number of columns.", group = "layout"}),

    awful.key({ modkey, "Control" }, "z",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "Decrease the number of columns.", group = "layout"}),

    awful.key({ modkey,           }, "Tab", function () awful.layout.inc( 1)                  end,
              {description = "Change layuot.", group = "layout"}),

    awful.key({ modkey, }, "m",
        function ()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal(
                    "request::activate", "key.unminimize", {raise = true}
                )
            end
        end,
        {description = "Restore minimized.", group = "client"})

)

-- CLient/wdindow options.
clientkeys = gears.table.join(
    awful.key({ modkey,           }, "x",      function (c) c.fullscreen = not c.fullscreen c:raise() end,
        {description = "Toggle fullscreen.",   group = "client"}),

    awful.key({ modkey, "Shift"   }, "r",      function (c) c:kill()                             end,
        {description = "Close.",               group = "client"}),

    awful.key({ modkey, "Shift"   }, "m",  awful.client.floating.toggle                         ,
        {description = "Toggle floating.",     group = "client"}),

    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster())     end,
        {description = "Move to master.",      group = "client"}),

    awful.key({ modkey,           }, "a",      function (c) c.ontop = not c.ontop                end,
        {description = "Toggle keep on top.",  group = "client"}),

    awful.key({ modkey,           }, "+",      function() gaps_resize(1) end,
        {description = "Increase gaps space.", group = "client"}),

    awful.key({ modkey,           }, "-",      function() gaps_resize(-1) end,
        {description = "Decrese gaps space.",  group = "client"}),

    awful.key({ modkey, "Control" }, "x", function ()
            for s in screen do
                s.mywibox.visible = not s.mywibox.visible
                if s.mybottomwibox then
                    s.mybottomwibox.visible = not s.mybottomwibox.visible
                end
            end
    end,
        {description = "Toggle wibox.", group = "awesome"}),

    awful.key({ modkey, "Shift"   }, "d",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"})
)

-- Bind all key numbers to tags.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

-- TODO: Make keys more custom applying a switch statment.

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end),

	-- Scroll middle button.
    awful.button({ }, 2, function (c)
        -- Figma.
        if awful.rules.match(c, { name = "Figma" }) then
        	awful.spawn("xdotool key Delete ")
    	end

        -- Image viewer.
        if awful.rules.match(c, { name = "^feh" }) then
        	awful.spawn("xdotoxol key Control_L+Delete ")
    	end

    end),

    -- Button 10 on Logitech g502.
    --awful.button({ }, 15, function (c)
         -- Meta key or Win key.
    --    awful.spawn("xdotool key 'Super_L' ")
    --end),

    -- Scroll left button.
    awful.button({ "Control" }, 6, function (c)
         -- Browsers
         -- Move 1 tab back.
        if awful.rules.match(c, { role = "browser" }) then
        	awful.spawn("xdotool key 'Prior' ")
    	end

        -- Disable cs2 cheat
        -- Does not works during gameplay.
        if awful.rules.match(c, { class = "cs2" }) then
        	awful.spawn("sudo pkill tux")
    	end
    end),

    -- Scroll right button.
    awful.button({ "Control" }, 7, function (c)
         -- Browsers
         -- Move 1 tab Next.
        if awful.rules.match(c, { role = "browser" }) then
        	awful.spawn("xdotool key 'Next' ")
    	end

        -- CS2 cheat.
        -- Does not works during gameplay.
        if awful.rules.match(c, { class = "cs2" }) then
        	awful.spawn("legit")
    	end

    end),

    -- Left side last button.
    -- Move to the next client.
    awful.button({ modkey }, 8, function ()
            awful.client.focus.byidx(1)
    end),

    awful.button({ }, 8, function (c)
        -- Figma
        if awful.rules.match(c, { name = "Figma" }) then
        	awful.spawn("xdotool key Delete ")
    	end

        -- Image viewer.
        if awful.rules.match(c, { name = "^feh" }) then
        	awful.spawn("xdotool key Down ")
    	end

        -- Browsers
        if awful.rules.match(c, { role = "browser" }) then
        	awful.spawn("xdotool key 'Control_L+t' ")
    	end
    end),

    -- Left side first button.
    awful.button({ }, 9, function (c)
        -- Figma.
        if awful.rules.match(c, { name = "Figma" }) then
        	awful.spawn("xdotool key i")
    	end

        -- Image viewer.
        if awful.rules.match(c, { name = "^feh" }) then
        	awful.spawn("xdotool key Up ")
    	end

         -- Browsers
        if awful.rules.match(c, { role = "browser" }) then
        	awful.spawn("xdotool key 'Control_L+w' ")
    	end

   end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                     minimized = false,
     }
    },

    {
        rule = { name = "Media viewer" },
        properties = { maximized = true, fullscreen = true, ontop = true }
    },
    {
        rule = { name = "Calculator" },
        properties = { ontop = true }
    },
    {
        rule = { class = "cs2" },
          properties = { border_width = 0, minimized = false }
    },
    {
        rule_any = {
            class = {
                "eww-player",
                "eww-quotes",
                "eww-sys",
            },
        }, properties = { floating = true, tag = "", border_width = 0 }
   },

    {
        rule = { name = "GLava" },
        properties = {
            floating = true,
            border_width = 0,
            x = 1350,
            y = 0,
        },
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",    -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "telegram-desktop",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Wpa_gui",
          "zoom",
          "GLava",
          },

        name = {
          "Event Tester",  -- xev.
          "Chat",
        },
        role = {
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

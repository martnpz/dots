GTK theme:
+ Widget: Orchis-Green-dark-compact.
___Donwload from gnome-look.org___
+ Icon Theme:   Win10-green.
                Win10-green-dark.
                Win11-green.
                Win11-green-dark.
                Fluent-green-dark.
+ Cursor: Vimix Cursors-White.
+ Font: Cantarell 11.

Terminal Font:
+ JetBrainsMono Nerd Font.
+ Hack Nerd Font.
